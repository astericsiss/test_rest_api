package specification;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class Specification {

    /**
     * Метод спецификации для запроса
     * @author Алехнович Александр
     */
    public  static RequestSpecification requestPostSpec(String baseURL) {
        return new RequestSpecBuilder()
                .setBaseUri(baseURL)
                .setContentType("application/json")
                .build();
    }

    /**
     * Метод спецификации для запроса
     * @author Алехнович Александр
     */
    public  static RequestSpecification requestGetSpec(String baseURL) {
        return new RequestSpecBuilder()
                .setBaseUri(baseURL)
                .build();

    }

    /**
     * Метод спецификации для ответа
     * @author Алехнович Александр
     */
    public static ResponseSpecification responseSpec(Integer statusCode){
        return new ResponseSpecBuilder()
                .expectStatusCode(statusCode)
                .build();
    }

    /**
     * Метод для установки спецификаций
     * @author Алехнович Александр
     */
    public static void installSpec(RequestSpecification requestSpec, ResponseSpecification responseSpec){
        RestAssured.requestSpecification = requestSpec;
        RestAssured.responseSpecification = responseSpec;
    }

    /**
     * Метод для удаления спецификаций
     * @author Алехнович Александр
     */
    public static void deleteSpec() {
        RestAssured.requestSpecification = null;
        RestAssured.responseSpecification = null;
    }
}
