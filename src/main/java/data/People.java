package data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class People {

    /**
     * Поле Integer id
     * @author Алехнович Александр
     */
    private Integer id;

    /**
     * Поле String email
     * @author Алехнович Александр
     */
    private String email;

    /**
     * Поле String first_name
     * @author Алехнович Александр
     */
    private String first_name;

    /**
     * Поле String last_name
     * @author Алехнович Александр
     */
    private String last_name;

    /**
     * Поле String avatar
     * @author Алехнович Александр
     */
    private String avatar;

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public People() {
        super();
    }

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public People(Integer id, String email, String first_name, String last_name, String avatar) {
        this.id = id;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.avatar = avatar;
    }

    /**
     * Метод чтения значения поля id
     * @author Алехнович Александр
     */
    public Integer getId() {
        return id;
    }

    /**
     * Метод присвоения значения полю id
     * @author Алехнович Александр
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Метод чтения значения поля email
     * @author Алехнович Александр
     */
    public String getEmail() {
        return email;
    }

    /**
     * Метод присвоения значения полю email
     * @author Алехнович Александр
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Метод чтения значения поля first_name
     * @author Алехнович Александр
     */
    @JsonProperty("first_name")
    public String getFirstName() {
        return first_name;
    }

    /**
     * Метод присвоения значения полю first_name
     * @author Алехнович Александр
     */
    @JsonProperty("first_name")
    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    /**
     * Метод чтения значения поля last_name
     * @author Алехнович Александр
     */
    @JsonProperty("last_name")
    public String getLastName() {
        return last_name;
    }

    /**
     * Метод присвоения значения полю last_name
     * @author Алехнович Александр
     */
    @JsonProperty("last_name")
    public void setLastName(String last_name) {
        this.last_name = last_name;
    }

    /**
     * Метод чтения значения поля avatar
     * @author Алехнович Александр
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Метод присвоения значения полю avatar
     * @author Алехнович Александр
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
