package data;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value= JsonInclude.Include.NON_ABSENT)
public class PeopleLoginRequest {

    /**
     * Поле String email
     * @author Алехнович Александр
     */
    private String email;

    /**
     * Поле String password
     * @author Алехнович Александр
     */
    private String password;

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public PeopleLoginRequest() {
        super();
    }

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public PeopleLoginRequest(String email) {
        this.email = email;
    }

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public PeopleLoginRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    /**
     * Метод чтения значения поля password
     * @author Алехнович Александр
     */
    public String getPassword() {
        return password;
    }

    /**
     * Метод присвоения значения полю password
     * @author Алехнович Александр
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Метод чтения значения поля email
     * @author Алехнович Александр
     */
    public String getEmail() {
        return email;
    }

    /**
     * Метод присвоения значения полю email
     * @author Алехнович Александр
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
