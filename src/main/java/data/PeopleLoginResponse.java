package data;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value= JsonInclude.Include.NON_ABSENT)
public class PeopleLoginResponse {

    /**
     * Поле String token
     * @author Алехнович Александр
     */
    private String token;

    /**
     * Поле String error
     * @author Алехнович Александр
     */
    private String error;

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public PeopleLoginResponse() {
        super();
    }

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public PeopleLoginResponse(String token, String error) {
        this.token = token;
        this.error = error;
    }

    /**
     * Метод чтения значения поля token
     * @author Алехнович Александр
     */
    public String getToken() {
        return token;
    }

    /**
     * Метод присвоения значения полю token
     * @author Алехнович Александр
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Метод чтения значения поля error
     * @author Алехнович Александр
     */
    public String getError() {
        return error;
    }

    /**
     * Метод присвоения значения полю error
     * @author Алехнович Александр
     */
    public void setError(String error) {
        this.error = error;
    }
}
