package data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResourcePeople {

    /**
     * Поле Integer id
     * @author Алехнович Александр
     */
    private Integer id;

    /**
     * Поле String name
     * @author Алехнович Александр
     */
    private String name;

    /**
     * Поле Integer year
     * @author Алехнович Александр
     */
    private Integer year;

    /**
     * Поле String color
     * @author Алехнович Александр
     */
    private String color;

    /**
     * Поле String pantone_value
     * @author Алехнович Александр
     */
    private String pantone_value;

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public ResourcePeople() {
        super();
    }

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public ResourcePeople(Integer id, String name, Integer year, String color, String pantone_value) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.color = color;
        this.pantone_value = pantone_value;
    }

    /**
     * Метод чтения значения поля id
     * @author Алехнович Александр
     */
    public Integer getId() {
        return id;
    }

    /**
     * Метод присвоения значения полю id
     * @author Алехнович Александр
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Метод чтения значения поля name
     * @author Алехнович Александр
     */
    public String getName() {
        return name;
    }

    /**
     * Метод присвоения значения полю name
     * @author Алехнович Александр
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Метод чтения значения поля year
     * @author Алехнович Александр
     */
    public Integer getYear() {
        return year;
    }

    /**
     * Метод присвоения значения полю year
     * @author Алехнович Александр
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /**
     * Метод чтения значения поля color
     * @author Алехнович Александр
     */
    public String getColor() {
        return color;
    }

    /**
     * Метод присвоения значения полю color
     * @author Алехнович Александр
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * Метод чтения значения поля pantone_value
     * @author Алехнович Александр
     */
    @JsonProperty("pantone_value")
    public String getPantoneValue() {
        return pantone_value;
    }

    /**
     * Метод присвоения значения полю pantone_value
     * @author Алехнович Александр
     */
    @JsonProperty("pantone_value")
    public void setPantoneValue(String pantone_value) {
        this.pantone_value = pantone_value;
    }
}
