package data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DataPeople {

    /**
     * Поле Integer page
     * @author Алехнович Александр
     */
    private Integer page;

    /**
     * Поле Integer per_page
     * @author Алехнович Александр
     */
    private Integer per_page;

    /**
     * Поле Integer total
     * @author Алехнович Александр
     */
    private Integer total;

    /**
     * Поле Integer total_pages
     * @author Алехнович Александр
     */
    private Integer total_pages;

    /**
     * Поле List<People> data
     * @author Алехнович Александр
     */
    private List<People> data;

    /**
     * Поле Support support
     * @author Алехнович Александр
     */
    private Support support;

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public DataPeople() {
        super();
    }

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public DataPeople(Integer page, Integer per_page, Integer total, Integer total_pages, List<People> data, Support support) {
        this.page = page;
        this.per_page = per_page;
        this.total = total;
        this.total_pages = total_pages;
        this.data = data;
        this.support = support;
    }

    /**
     * Метод чтения значения поля page
     * @author Алехнович Александр
     */
    public Integer getPage() {
        return page;
    }

    /**
     * Метод присвоения значения полю page
     * @author Алехнович Александр
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * Метод чтения значения поля per_page
     * @author Алехнович Александр
     */
    @JsonProperty("per_page")
    public Integer getPerPage() {
        return per_page;
    }

    /**
     * Метод присвоения значения полю per_page
     * @author Алехнович Александр
     */
    @JsonProperty("per_page")
    public void setPerPage(Integer per_page) {
        this.per_page = per_page;
    }

    /**
     * Метод чтения значения поля total
     * @author Алехнович Александр
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * Метод присвоения значения полю total
     * @author Алехнович Александр
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * Метод чтения значения поля total_pages
     * @author Алехнович Александр
     */
    @JsonProperty("total_pages")
    public Integer getTotalPages() {
        return total_pages;
    }

    /**
     * Метод присвоения значения полю total_pages
     * @author Алехнович Александр
     */
    @JsonProperty("total_pages")
    public void setTotalPages(Integer total_pages) {
        this.total_pages = total_pages;
    }

    /**
     * Метод чтения значения поля data
     * @author Алехнович Александр
     */
    public List<People> getData() {
        return data;
    }

    /**
     * Метод присвоения значения полю data
     * @author Алехнович Александр
     */
    public void setData(List<People> data) {
        this.data = data;
    }

    /**
     * Метод чтения значения поля support
     * @author Алехнович Александр
     */
    public Support getSupport() {
        return support;
    }

    /**
     * Метод чтения значения поля support
     * @author Алехнович Александр
     */
    public void setSupport(Support support) {
        this.support = support;
    }
}
