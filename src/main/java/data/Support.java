package data;

public class Support {

    /**
     * Поле String url
     * @author Алехнович Александр
     */
    private String url;

    /**
     * Поле String text
     * @author Алехнович Александр
     */
    private String text;

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public Support(){
        super();
    }

    /**
     * Конструктор класса
     * @author Алехнович Александр
     */
    public Support(String url, String text) {
        this.url = url;
        this.text = text;
    }

    /**
     * Метод чтения значения поля url
     * @author Алехнович Александр
     */
    public String getUrl() {
        return url;
    }

    /**
     * Метод присвоения значения полю url
     * @author Алехнович Александр
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Метод чтения значения поля text
     * @author Алехнович Александр
     */
    public String getText() {
        return text;
    }

    /**
     * Метод присвоения значения полю text
     * @author Алехнович Александр
     */
    public void setText(String text) {
        this.text = text;
    }
}
