package properties.interfaces;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({"file:src/main/resources/properties/url.properties",
        "system:properties",
        "system:env",})
public interface UrlProperties extends Config {

    /**
     * Метод константы адрес сайта
     * @author Алехнович Александр
     */
    @Config.Key("reqres.url")
    String reqresUrl();
}
