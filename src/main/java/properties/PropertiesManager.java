package properties;

import org.aeonbits.owner.ConfigFactory;
import properties.interfaces.UrlProperties;

public class PropertiesManager {

    /**
     * Инициализация класса UrlProperties
     * @author Алехнович Александр
     */
    public static UrlProperties urlProperties = ConfigFactory.create(UrlProperties.class);
}
