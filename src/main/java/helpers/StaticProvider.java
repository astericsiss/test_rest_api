package helpers;

import data.PeopleLoginRequest;
import org.testng.annotations.DataProvider;
import java.lang.reflect.Method;

public class StaticProvider {

    /**
     * Данные для тестирования https://reqres.in/
     * @author Алехнович Александр
     */
    @DataProvider(name = "reqres")
    public static Object [][] dataReqres(Method param) {
        switch (param.getName()) {
            case "getFirstNameUsersTest":
                return new Object[][] {{"/api/users?page=2",200}};
            case "loginTest":
                return new Object[][] {{new PeopleLoginRequest("eve.holt@reqres.in", "cityslicka"),"/api/login","token",200}};
            case "notLoginTest":
                return new Object[][] {{new PeopleLoginRequest("peter@klaven"),"error","Missing password","/api/login",400}};
            case "resourceTest":
                return new Object[][] {{"/api/unknown",200}};
        }
        return null;
    }
}
