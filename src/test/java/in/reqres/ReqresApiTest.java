package in.reqres;

import baseTest.BaseTest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.DataPeople;
import data.PeopleLoginRequest;
import data.PeopleLoginResponse;
import data.Resource;
import helpers.StaticProvider;
import io.qameta.allure.Description;
import org.testng.annotations.Test;
import properties.PropertiesManager;

import java.util.*;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.*;
import static specification.Specification.*;

public class ReqresApiTest extends BaseTest {

    /**
     * Тест проверяет имена файлов аватаров пользователей на уникальность
     *
     * @author Алехнович Александр
     */
    @Description("Проверка данных зарегистрированных пользователей")
    @Test(groups = "reqres", dataProvider = "reqres", dataProviderClass = StaticProvider.class)
    void getFirstNameUsersTest(String path, int status) {
        installSpec(requestGetSpec(PropertiesManager.urlProperties.reqresUrl()), responseSpec(status));
        DataPeople dataPeople = given()
                .when()
                .get(path)
                .then()
                .log().all()
                .extract()
                .body()
                .as(DataPeople.class);
        assertNotEquals(dataPeople.getData().size(), 0, "Данные отсутствуют");
        Set<String> uniqueNames = new HashSet<>();
        List<String> avatarFirstName = new ArrayList<>();
        dataPeople.getData().forEach(x -> uniqueNames.add(x.getAvatar().substring(x.getAvatar().lastIndexOf("/") + 1, x.getAvatar().lastIndexOf("."))));
        dataPeople.getData().forEach(x -> avatarFirstName.add(x.getAvatar().substring(x.getAvatar().lastIndexOf("/") + 1, x.getAvatar().lastIndexOf("."))));
        assertEquals(avatarFirstName.size(), uniqueNames.size(), "Имена файлов аватаров пользователей не уникальны");
    }

    /**
     * Тест проверяет авторизацию пользователя в системе с корректным логином и паролем
     *
     * @author Алехнович Александр
     */
    @Description("Авторизация пользователя в системе с корректным логином и паролем")
    @Test(groups = "reqres", dataProvider = "reqres", dataProviderClass = StaticProvider.class)
    void loginTest(PeopleLoginRequest peopleLoginRequest, String path, String jsonKey, int status) {
        installSpec(requestPostSpec(PropertiesManager.urlProperties.reqresUrl()), responseSpec(status));
        PeopleLoginResponse peopleLoginResponse = given()
                .body(peopleLoginRequest)
                .post(path)
                .then()
                .log().all()
                .extract()
                .as(PeopleLoginResponse.class);
        Map<String, String> mapObj = new ObjectMapper().convertValue(peopleLoginResponse, new TypeReference<Map<String, String>>() {
        });
        assertNotEquals(mapObj.size(), 0, "Пришел пустой ответ");
        assertTrue(mapObj.entrySet().stream().anyMatch(x -> x.getKey().equals(jsonKey)), "Пользователь не авторизирован. В ответе нет ключа: " + jsonKey +
                ", вместо него пришел ответ: " + mapObj.entrySet().stream().findFirst().get().getKey());
        assertFalse(mapObj.entrySet().stream().filter(x -> x.getKey().equals(jsonKey)).findAny().get().getValue().isBlank(),
                "Тело ключа " + jsonKey + " в ответе пустое");
    }

    /**
     * Тест проверяет авторизацию пользователя в системе с логином и без пароля
     *
     * @author Алехнович Александр
     */
    @Description("Авторизация пользователя в системе с логином и без пароля")
    @Test(groups = "reqres", dataProvider = "reqres", dataProviderClass = StaticProvider.class)
    void notLoginTest(PeopleLoginRequest peopleLoginRequest, String jsonKey, String message, String path, int status) {
        installSpec(requestPostSpec(PropertiesManager.urlProperties.reqresUrl()), responseSpec(status));
        PeopleLoginResponse peopleLoginResponse = given()
                .body(peopleLoginRequest)
                .post(path)
                .then()
                .log().all()
                .extract()
                .body()
                .as(PeopleLoginResponse.class);
        Map<String, String> mapObj = new ObjectMapper().convertValue(peopleLoginResponse, new TypeReference<Map<String, String>>() {
        });
        assertNotEquals(mapObj.size(), 0, "Пришел пустой ответ");
        assertTrue(mapObj.entrySet().stream().anyMatch(x -> x.getKey().equals(jsonKey)), "В ответе нет ключа: " + jsonKey +
                ", вместо него пришел ответ: " + mapObj.entrySet().stream().findFirst().get().getKey());
        assertEquals(mapObj.entrySet().stream().filter(x -> x.getKey().equals(jsonKey)).findFirst().get().getValue(), message,
                "Тело ключа " + jsonKey + " в ответе не соответствует сообщению: " + message);
    }

    /**
     * Тест проверяет сортировку данных по годам
     *
     * @author Алехнович Александр
     */
    @Description("Проверка сортировки данных")
    @Test(groups = "reqres", dataProvider = "reqres", dataProviderClass = StaticProvider.class)
    void resourceTest(String path, int status) {
        installSpec(requestGetSpec(PropertiesManager.urlProperties.reqresUrl()), responseSpec(status));
        Resource people = given()
                .when()
                .get(path)
                .then()
                .log().all()
                .extract()
                .body()
                .as(Resource.class);
        List<Integer> years = new ArrayList<>();
        people.getData().forEach(x -> years.add(x.getYear()));
        assertNotEquals(years.size(), 0, "Данные отсутствуют");
        assertEquals(years.stream().sorted().collect(Collectors.toList()), years, "Данные не отсортированы по годам");
    }
}
