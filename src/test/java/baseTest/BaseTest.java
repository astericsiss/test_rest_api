package baseTest;

import org.testng.annotations.*;
import static specification.Specification.*;

public class BaseTest {

    /**
     * После каждого теста группы: "reqres" удаляет спецификации
     * @author Алехнович Александр
     */
    @AfterMethod(groups = {"reqres"})
    void delete() {
        deleteSpec();
    }
}
